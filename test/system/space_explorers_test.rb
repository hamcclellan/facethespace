require "application_system_test_case"

class SpaceExplorersTest < ApplicationSystemTestCase
  setup do
    @space_explorer = space_explorers(:one)
  end

  test "visiting the index" do
    visit space_explorers_url
    assert_selector "h1", text: "Space Explorers"
  end

  test "creating a Space explorer" do
    visit space_explorers_url
    click_on "New Space Explorer"

    click_on "Create Space explorer"

    assert_text "Space explorer was successfully created"
    click_on "Back"
  end

  test "updating a Space explorer" do
    visit space_explorers_url
    click_on "Edit", match: :first

    click_on "Update Space explorer"

    assert_text "Space explorer was successfully updated"
    click_on "Back"
  end

  test "destroying a Space explorer" do
    visit space_explorers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Space explorer was successfully destroyed"
  end
end
