require 'test_helper'

class SpaceExplorersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @space_explorer = space_explorers(:one)
  end

  test "should get index" do
    get space_explorers_url
    assert_response :success
  end

  # test "should get new" do
  #   get new_space_explorer_url
  #   assert_response :success
  # end

  # test "should create space_explorer" do
  #   assert_difference('SpaceExplorer.count') do
  #     post space_explorers_url, params: { space_explorer: {  } }
  #   end
  #
  #   assert_redirected_to space_explorer_url(SpaceExplorer.last)
  # end

  test "should show space_explorer" do
    get space_explorer_url(@space_explorer)
    assert_response :success
  end

  # test "should get edit" do
  #   get edit_space_explorer_url(@space_explorer)
  #   assert_response :success
  # end

  # test "should update space_explorer" do
  #   patch space_explorer_url(@space_explorer), params: { space_explorer: {  } }
  #   assert_redirected_to space_explorer_url(@space_explorer)
  # end

  # test "should destroy space_explorer" do
  #   assert_difference('SpaceExplorer.count', -1) do
  #     delete space_explorer_url(@space_explorer)
  #   end

    assert_redirected_to space_explorers_url
  end
