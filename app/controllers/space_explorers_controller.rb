class SpaceExplorersController < ApplicationController

  include SpaceExplorersHelper
  @api_uri = "https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space/1.0.0/m"
  # before_action :set_space_explorer, only: [:show, :edit, :update, :destroy]

  # GET /space_explorers
  # GET /space_explorers.json
  def index

    resp= get_all_spaces()
    @space = resp=='err'? 'err' : resp['items'][0]
    @entries = get_all_entries(@space['sys']['id'])
    @entries_table_fieldnames = {'fields': ['title','summary',''],'sys':['createdAt','createdBy','updatedAt','updatedBy']}
    @assets_table_fieldnames = {'fields':  ['title', 'contentType', 'fileName', 'upload'], 'sys': ['createdAt', 'createdBy', 'updatedAt']}
    @assets = get_all_assets(@space['sys']['id'])
  end

  # GET /space_explorers/1
  # GET /space_explorers/1.json
  def show
    resp= get_all_spaces()
    @space = resp=='err'? 'err' : resp['items'][0]
    # @space = get_all_spaces()['items'][0]
    @entries = get_all_entries(params[:id])
    @entries_table_fieldnames = {'fields': ['title','summary',''],'sys':['createdAt','createdBy','updatedAt','updatedBy']}
    @assets_table_fieldnames = {'fields':  ['title', 'contentType', 'fileName', 'upload'], 'sys': ['createdAt', 'createdBy', 'updatedAt']}
    @assets = get_all_assets(params[:id])

  end

  # GET /space_explorers/new
  def new
    @space_explorer = SpaceExplorer.new
  end

  # GET /space_explorers/1/edit
  def edit
  end

  # POST /space_explorers
  # POST /space_explorers.json
  def create
    @space_explorer = SpaceExplorer.new(space_explorer_params)

    respond_to do |format|
      if @space_explorer.save
        format.html { redirect_to @space_explorer, notice: 'Space explorer was successfully created.' }
        format.json { render :show, status: :created, location: @space_explorer }
      else
        format.html { render :new }
        format.json { render json: @space_explorer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /space_explorers/1
  # PATCH/PUT /space_explorers/1.json
  def update
    respond_to do |format|
      if @space_explorer.update(space_explorer_params)
        format.html { redirect_to @space_explorer, notice: 'Space explorer was successfully updated.' }
        format.json { render :show, status: :ok, location: @space_explorer }
      else
        format.html { render :edit }
        format.json { render json: @space_explorer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /space_explorers/1
  # DELETE /space_explorers/1.json
  def destroy
    @space_explorer.destroy
    respond_to do |format|
      format.html { redirect_to space_explorers_url, notice: 'Space explorer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_space_explorer
      @space_explorer = SpaceExplorer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def space_explorer_params
      params.fetch(:space_explorer, {})
    end
end
