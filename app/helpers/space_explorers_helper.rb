require 'uri'
require 'net/http'
require 'json'

module SpaceExplorersHelper
  API_uri = "https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space/3.0.1/m"

  Test_uri="https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/aa4e5e49-3abe-4ec6-861b-b1edc0508cd3/space/2.0.0/m"

  def spaces_get(url)
    result = Net::HTTP.get_response(URI(url))
    return result
  end

  def get_all_spaces()
    full_url = API_uri + '/space'
    space_data = spaces_get(full_url)
    begin
      if space_data.is_a?(Net::HTTPSuccess) then
      return JSON.parse(space_data.read_body.as_json)
      else
        return 'err'
      end
    rescue
      return 'err'
    end
  end

  def get_space_by_id(id)
    full_url = API_uri + '/space/' + id.to_s
    space_data = spaces_get(full_url)
    begin
      if space_data.is_a?(Net::HTTPSuccess) then
      return JSON.parse(space_data.read_body.as_json)
      else
        return 'err'
      end
    rescue
      return 'err'
    end
  end

  def get_all_entries(space_id)
    full_url = API_uri+'/space/'+space_id.to_s+'/entries'
    entries_data = spaces_get(full_url)
    begin
      if entries_data.is_a?(Net::HTTPSuccess) then
      return JSON.parse(entries_data.read_body.as_json)
      else
        return 'err'
      end
    rescue
      return 'err'
    end
  end

  def get_user_by_id(user_id)
    full_url=API_uri+'/users/'+user_id.to_s
    user_data = spaces_get(full_url)
    begin
      if user_data.is_a?(Net::HTTPSuccess) then
      return JSON.parse(user_data.read_body.as_json)
      else
        return 'err'
      end
    rescue
      return 'err'
    end
  end

  def split_camel_casing(text)
    words_array = text.split /(?=[A-Z])/
    return words_array*" "
  end

  def get_all_assets(space_id)
    full_url = API_uri+'/space/'+space_id.to_s+'/assets'
    assets_data = spaces_get(full_url)
    begin
      if assets_data.is_a?(Net::HTTPSuccess) then
      return JSON.parse(assets_data.read_body.as_json)
      else
        return 'err'
      end
    rescue
      return 'err'
    end
  end


end
