json.extract! space_explorer, :id, :created_at, :updated_at
json.url space_explorer_url(space_explorer, format: :json)
