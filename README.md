# README
Welcome to faceTheSpace, an original Ruby On Rails implementation of Spaces

* This project uses Ruby 2.4.2 and Rails 5.2.3

Installation guide:
* To Install Ruby on Rails: http://installrails.com/
* To ensure all dependencies are installed run bundle install

Notes:
* The API spec contained a missing comma preventing the spaces/ endpoint from returning data ; the updated .raml is hosted on MuleSoft and the original .raml can be tested against by changing the spaces_explorers_controller to use Test_uri instead of API_uri
* Run Unit tests from the Command Line:
  * rake test

Next features:
* Implement DataTables for delayed view rendering
* Convert Index and Show to render a partial view
* More unit testing
* Add nicer stylesheets
* Add authentication to the spaces API
